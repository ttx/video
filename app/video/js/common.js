var CONFIG = {
	LOGIN_KEY: "LOGIN_KEY",
	IOS: 1,
	ANDROID: 2,
	DOMAIN: "http://116.255.196.159:10002"
	//	DOMAIN: "http://192.168.0.83:8080"
}

function requestUrl(url) {
	return CONFIG.DOMAIN + url;
}

function getSrial() {
	if(mui.os.ios) {
		return ""
	} else {
		var build = plus.android.importClass("android.os.Build");
		return build.SERIAL;
	}
}

function setLoginUser(user) {
	plus.storage.setItem(CONFIG.LOGIN_KEY, JSON.stringify(user));
}

function getLoginUser() {
	var jsonUser = plus.storage.getItem(CONFIG.LOGIN_KEY);
	if(jsonUser) {
		return JSON.parse(jsonUser)
	} else {
		return false;
	}
}

function logout() {
	plus.storage.removeItem(CONFIG.LOGIN_KEY);
}

function postServer(para) {
	var user = getLoginUser();
	var wait = undefined;
	$.ajax($.extend({
		type: "post",
		timeout: 10000,
		beforeSend: function(request) {
			wait = plus.nativeUI.showWaiting();
			if(user) {
				request.setRequestHeader("SESSION_APPUSER", JSON.stringify(user));
			}
		},
		complete: function() {
			wait && wait.close();
		},
		error: function(e) {
			console.log(JSON.stringify(e));
			wait.close();
			mui.toast("网络繁忙...")
		},
		contentType: "application/json; charset=utf-8",
		dataType: "json"
	}, para));
}

function postNowaitServer(para) {
	var user = getLoginUser();
	$.ajax($.extend({
		type: "post",
		timeout: 10000,
		beforeSend: function(request) {
			if(user) {
				request.setRequestHeader("SESSION_APPUSER", JSON.stringify(user));
			}
		},
		complete: function() {},
		error: function(e) {},
		contentType: "application/json; charset=utf-8",
		dataType: "json"
	}, para));
}

Date.prototype.format = function(fmt) {
	var o = {
		"M+": this.getMonth() + 1, //月份 
		"d+": this.getDate(), //日 
		"h+": this.getHours(), //小时 
		"m+": this.getMinutes(), //分 
		"s+": this.getSeconds(), //秒 
		"q+": Math.floor((this.getMonth() + 3) / 3), //季度 
		"S": this.getMilliseconds() //毫秒 
	};
	if(/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	}
	for(var k in o) {
		if(new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
}

function checkVersion() {
	//检查更新
	plus.runtime.getProperty(plus.runtime.appid, function(inf) {
		//		alert("当前的版本:" + inf.version)
		postServer({
			url: requestUrl("/app/appUp/findAppUpNew"),
			success: function(result) {
				if(result.success) {
					if(inf.version == undefined || result.result == undefined) return;
					if(result.result.version !== inf.version) {
						var update = function() {
							mui.alert('发现新版本', '更新消息!', function() {
								if(result.result.anew) {
									plus.runtime.openURL(requestUrl("/app/file/download/" + result.result.fileId));
									return;
								}
								var download = plus.downloader.createDownload(requestUrl("/app/file/download/" + result.result.fileId), {
									filename: "_doc/update/" + result.result.fileId + ".wgt"
								});

								var wait = plus.nativeUI.showWaiting("0%");
								download.addEventListener("statechanged", function(d, status) {
									switch(d.state) {
										case 1: // 开始
											break;
										case 2: //已连接到服务器
											break;
										case 3: // 已接收到数据
											var current = parseInt(100 * d.downloadedSize / d.totalSize);
											var rate = " " + current + "% ";
											wait.setTitle(rate);
											break;
										case 4: // 下载完成 
											plus.runtime.install(d.filename, {}, function() {
												wait.close();
												plus.nativeUI.alert("更新完成,立即重启", function() {
													plus.runtime.restart();
												});
											}, function(e) {
												alert("安装失败!");
												wait.close();
												console.log("安装wgt文件失败[" + e.code + "]：" + e.message);
												plus.nativeUI.alert("安装wgt文件失败[" + e.code + "]：" + e.message);
											});
											break;
									}
								})
								download.start();

							});
						}
						if(mui.os.ios && ((CONFIG.IOS | result.result.platform) == result.result.platform)) {
							update();
							return;
						} else if((!mui.os.ios) && ((CONFIG.ANDROID | result.result.platform) == result.result.platform)) {
							update();
							return;
						} else {
							mui.toast("已是最新!")
						}

					} else {
						mui.toast("已是最新!")
					}
				}
			}
		})
	});
}