package com.admin.test;

import com.admin.enums.Constant;
import com.admin.model.*;
import com.admin.repository.mongo.*;
import com.admin.service.EmailService;
import com.alibaba.fastjson.JSONObject;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

public class DataGenTest extends BaseTest {

    @Resource
    VideoSupplierRepository videoSupplierRepository;

    @Resource
    AppUserRepository appUserRepository;


    @Resource
    ActivationCodeRepository activationCodeRepository;

    @Test
    public void findAppUser() {
        AppUser byEmailAndPassword = appUserRepository.findByEmailAndPassword("422450455@qq.com", "59c81b4bfc337cd8a11c4de6e31eda2d");
        System.out.println(JSONObject.toJSONString(byEmailAndPassword));
    }

    @Test
    public void addAppUser() {
        AppUser appUser = new AppUser();
        appUser.setPassword(DigestUtils.md5Hex("weimei"));
        appUser.setEmail("422450455@qq.com");
        appUser.setHead("111");
        appUser.setMember(true);
        appUser.setNickname("汤圆");
        appUser.setPhone("18583382048");
        appUserRepository.save(appUser);
    }

    @Resource
    MongoTemplate mongoTemplate;

    @Test
    public void getTest() {
        List<AppUser> id = mongoTemplate.find(Query.query(Criteria.where("_id").is("5a9c06c3acc4acca33f4a5e9")), AppUser.class);
        System.out.println(id.size());
    }

    @Resource
    private WatchHistoryRepository watchHistoryRepository;

    Random r = new Random();

    @Test
    public void watchGen() {
        List<WatchHistory> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            WatchHistory watchHistory = new WatchHistory();
            watchHistory.setWatchTime(System.currentTimeMillis() + r.nextInt(1000 * 60 * 60 * 24));
            watchHistory.setTitle("最强大脑之燃烧吧大脑_20180302期-最强大脑之燃烧吧大脑-综艺-高清正版视频–爱奇艺" + i);
            watchHistory.setAppUserId("5aa14e1bacc4209248628e5f");
            list.add(watchHistory);
        }
        watchHistoryRepository.save(list);

    }

    @Test
    public void genVideoSupplier() {
        videoSupplierRepository.deleteAll();
        List<VideoSupplier> list = new ArrayList<>();
        VideoSupplier aiqiyi = new VideoSupplier();


        VideoSupplier tengxun = new VideoSupplier();
        tengxun.setLabel("腾讯视频");
        tengxun.setIcon("images/tengxun.png");
        tengxun.setUrl("http://m.v.qq.com/");
        tengxun.setScript("$(\".site_player\").removeClass(\"site_player\").html(\"<img src='http://116.255.196.159:10002/app/file/view/5aacaa36acc476cf66684835'>\").click(function(){plus.webview.currentWebview().opener().evalJS(\"window.play()\");})");
        tengxun.setInterfaceUrl("http://api.iy11.cn/apiget.php?url=");
        tengxun.getConvertMap().put("https://m,v,qq,com/", "https://v.qq.com/");
        tengxun.setSupport(true);
        list.add(tengxun);


        aiqiyi.setLabel("爱奇艺");
        aiqiyi.setIcon("images/aiqiyi.png");
        aiqiyi.setUrl("http://m.iqiyi.com/");
        aiqiyi.setScript("$(\".m-video-player\").removeClass(\"m-video-player\").html(\"<img src='http://116.255.196.159:10002/app/file/view/5aacaa36acc476cf66684835'>\").click(function(){plus.webview.currentWebview().opener().evalJS(\"window.play()\");})");
        aiqiyi.setInterfaceUrl("http://api.iy11.cn/apiget.php?url=");
        aiqiyi.getConvertMap().put("http://m,iqiyi,com/", "http://www.iqiyi.com/");
        aiqiyi.setSupport(true);
        List<BackupInterface> backupInterfaces = aiqiyi.getBackupInterfaces();
        backupInterfaces.add(new BackupInterface("线路1","http://www.52cbg.com/4.php?url="));
        backupInterfaces.add(new BackupInterface("线路2","http://api.baiyug.cn/vip/index.php?url="));
        backupInterfaces.add(new BackupInterface("线路3","http://tv.x-99.cn/api/wnapi.php?id="));
        backupInterfaces.add(new BackupInterface("1717","http://www.1717yun.com/jx/ty.php?url="));


        list.add(aiqiyi);





        VideoSupplier youku = new VideoSupplier();
        youku.setLabel("优酷视频");
        youku.setIcon("images/youku.png");
        youku.setUrl("http://www.youku.com/");
        youku.setScript("document.getElementById(\"player\").innerHTML = \"<img src='http://116.255.196.159:10002/app/file/view/5aacaa36acc476cf66684835'>\";document.getElementById(\"player\").onclick = function(){plus.webview.currentWebview().opener().evalJS(\"window.play()\");}");
        youku.setInterfaceUrl("http://api.iy11.cn/apiget.php?url=");
        youku.getConvertMap().put("http://m,youku,com/", "http://v.youku.com/");
        youku.setSupport(false);
        list.add(youku);



        VideoSupplier souhu = new VideoSupplier();
        souhu.setLabel("搜狐视频");
        souhu.setIcon("images/souhu.png");
        souhu.setUrl("https://m.tv.sohu.com/film");
        souhu.setScript("$(\".player,#top-poster\").html(\"<img src='http://116.255.196.159:10002/app/file/view/5aacaa36acc476cf66684835'>\").click(function(){plus.webview.currentWebview().opener().evalJS(\"window.play()\");})");
        souhu.setInterfaceUrl("http://api.iy11.cn/apiget.php?url=");
        souhu.getConvertMap().put("https://m,tv,sohu,com/", "https://tv.sohu.com/");
        souhu.getConvertMap().put("https://m,film,sohu,com/", "https://film.sohu.com/");
        souhu.setSupport(false);
        list.add(souhu);

        VideoSupplier pptv = new VideoSupplier();
        pptv.setLabel("PPTV");
        pptv.setIcon("images/pptv.png");
        pptv.setUrl("http://m.pptv.com/?f=pptv&f=pptv");
        pptv.setScript("$(\"#playerbox\").html(\"<img src='http://116.255.196.159:10002/app/file/view/5aacaa36acc476cf66684835'>\").click(function(){plus.webview.currentWebview().opener().evalJS(\"window.play()\");})");
        pptv.setInterfaceUrl("http://vip.jlsprh.com/index.php?url=");
        pptv.getConvertMap().put("http://m,pptv,com/","http://v.pptv.com/");
        pptv.setSupport(true);
        list.add(pptv);
        
        
        VideoSupplier leshi = new VideoSupplier();
        leshi.setLabel("乐视视频");
        leshi.setIcon("images/leshi.png");
        leshi.setUrl("http://m.pptv.com/?f=pptv&f=pptv");
        leshi.setScript("$(\"#playerbox\").html(\"<img src='http://116.255.196.159:10002/app/file/view/5aacaa36acc476cf66684835'>\").click(function(){plus.webview.currentWebview().opener().evalJS(\"window.play()\");})");
        leshi.setInterfaceUrl("http://api.iy11.cn/apiget.php?url=");
        leshi.getConvertMap().put("http://m,pptv,com/","http://v.pptv.com/");
        leshi.setSupport(false);
        list.add(leshi);
        
        
        VideoSupplier mangguo = new VideoSupplier();
        mangguo.setLabel("芒果视频");
        mangguo.setIcon("images/mangguo.png");
        mangguo.setUrl("http://m.pptv.com/?f=pptv&f=pptv");
        mangguo.setScript("$(\"#playerbox\").html(\"<img src='http://116.255.196.159:10002/app/file/view/5aacaa36acc476cf66684835'>\").click(function(){plus.webview.currentWebview().opener().evalJS(\"window.play()\");})");
        mangguo.setInterfaceUrl("http://api.iy11.cn/apiget.php?url=");
        mangguo.getConvertMap().put("http://m,pptv,com/","http://v.pptv.com/");
        mangguo.setSupport(false);
        list.add(mangguo);

        VideoSupplier tudou = new VideoSupplier();
        tudou.setLabel("芒果视频");
        tudou.setIcon("images/tudou.png");
        tudou.setUrl("http://m.pptv.com/?f=pptv&f=pptv");
        tudou.setScript("$(\"#playerbox\").html(\"<img src='http://116.255.196.159:10002/app/file/view/5aacaa36acc476cf66684835'>\").click(function(){plus.webview.currentWebview().opener().evalJS(\"window.play()\");})");
        tudou.setInterfaceUrl("http://api.iy11.cn/apiget.php?url=");
        tudou.getConvertMap().put("http://m,pptv,com/","http://v.pptv.com/");
        tudou.setSupport(false);
        list.add(tudou);

        videoSupplierRepository.save(list);
    }

    @Resource
    AppUpRepository appUpRepository;

    @Test
    public void versionUp() throws FileNotFoundException {
        AppUp appUp = new AppUp();
        appUp.setCreateTime(System.currentTimeMillis());
//        appUp.setVersion(UUID.randomUUID().toString());
        appUp.setVersion("2.6");
        DBObject metadata = new BasicDBObject();
        metadata.put("type", "wgt");
        appUp.setAnew(false);
        appUp.setPlatform(Constant.IOS | Constant.ANDROID);
        appUp.setFileId(gridFsTemplate.store(new FileInputStream("C:\\Users\\Administrator\\Desktop\\H54311C05.wgt"), "H54311C05.wgt", metadata).getId().toString());
        appUpRepository.save(appUp);
    }

    @Resource
    GridFsTemplate gridFsTemplate;

    @Test
    public void update() throws FileNotFoundException {
        DBObject metadata = new BasicDBObject();
        metadata.put("type", "wgt");
        GridFSFile store = gridFsTemplate.store(new FileInputStream("C:\\Users\\Administrator\\Desktop\\H54311C05.wgt"), "H54311C05.wgt", metadata);
        System.out.println(store.getId());
    }
    @Test
    public void update1() throws FileNotFoundException {
        DBObject metadata = new BasicDBObject();
        metadata.put("type", "img");
        GridFSFile store = gridFsTemplate.store(new FileInputStream("C:\\Users\\Administrator\\Desktop\\pay.PNG"), "pay.PNG", metadata);
        System.out.println(store.getId());
    }


    @Test
    public void clearWgt(){
        List<GridFSDBFile> gridFSDBFiles = gridFsTemplate.find(Query.query(Criteria.where("metadata.type").is("wgt")));
        for (GridFSDBFile gridFSDBFile : gridFSDBFiles) {
            gridFsTemplate.delete(Query.query(Criteria.where("_id").is(gridFSDBFile.getId())));
        }
//        System.err.println(gridFSDBFiles.size());
    }

    @Test
    public void payP() throws FileNotFoundException {
        DBObject metadata = new BasicDBObject();
        metadata.put("type", "img");
        metadata.put("role", "pay");
        gridFsTemplate.store(new FileInputStream("C:\\Users\\Administrator\\Desktop\\pay.PNG"), "pay.PNG", metadata);
    }

    @Resource
    EmailService emailService;
    @Test
    public void send() throws InterruptedException {
        emailService.sendSimpleMail("422450455@qq.com","测试","这是测试内容!");
        System.out.println("发送完毕");
        Thread.sleep(Integer.MAX_VALUE);
    }
}

