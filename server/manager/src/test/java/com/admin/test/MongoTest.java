package com.admin.test;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.Sphere;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;

import com.admin.model.TestModel;
import com.admin.repository.mongo.TestModelRepository;
import com.alibaba.fastjson.JSONObject;

public class MongoTest extends BaseTest {
    @Resource
    MongoTemplate mongoTemplate;

    @Resource
    TestModelRepository testRepository;

    @Test
    public void test() {
        TestModel testModel = new TestModel();

        testModel.setId(null);
        testModel.setName("通州");
        testModel.setLocation(new double[] { 116.655816, 39.909112 });
        // testModel.setPoint(new GeoJsonPoint( 116.663556, 39.915599 ));
        System.err.println(testRepository.save(testModel).getId());
        testModel.setId(null);

        testModel.setName("朝阳");
        testModel.setLocation(new double[] { 116.442613, 39.921488 });
        // testModel.setPoint(new GeoJsonPoint( 116.448537, 39.926666 ));
        System.err.println(testRepository.save(testModel).getId());

        testModel.setId(null);
        testModel.setName("东城");
        testModel.setLocation(new double[] { 116.417893, 39.926754 });
        // testModel.setPoint(new GeoJsonPoint( 116.420366, 39.935519 ));
        System.err.println(testRepository.save(testModel).getId());

        testModel.setId(null);
        testModel.setName("西城");
        testModel.setLocation(new double[] { 116.365365, 39.912009 });
        // testModel.setPoint(new GeoJsonPoint( 116.369774, 39.919141 ));
        System.err.println(testRepository.save(testModel).getId());

    }

    @Test
    public void testFind() throws Exception {

        Point point = new Point(116.417893, 39.926754);
        Sphere sphere = new Sphere(point, 2.2 * 0.6213712 / 3963.191);

        List<TestModel> venues = mongoTemplate.find(new Query(Criteria.where("location").within(sphere)), TestModel.class);
        for (TestModel testModel : venues) {
            System.out.println(JSONObject.toJSONString(testModel));
        }
    }

    @Test
    public void testFind1() throws Exception {
        // Circle circle = new Circle(116.369774, 39.919141, 0.2);
        // List<TestModel> venues = mongoTemplate.find(new
        // Query(Criteria.where("location").within(circle)), TestModel.class);

        Point point = new Point(116.369774, 39.919141);
        List<TestModel> venues = mongoTemplate.find(new Query(Criteria.where("location").near(point).maxDistance(0.01)), TestModel.class);
        for (TestModel testModel : venues) {
            System.out.println(JSONObject.toJSONString(testModel));
        }
    }

    @Test
    public void testFind2() throws Exception {
        Point location = new Point(116.369774, 39.919141);
        NearQuery query = NearQuery.near(location).maxDistance(new Distance(100000000, Metrics.MILES));
        GeoResults<TestModel> result = mongoTemplate.geoNear(query, TestModel.class);
        System.out.println(result.getContent().size());
    }

}
