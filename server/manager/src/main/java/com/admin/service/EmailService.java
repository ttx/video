package com.admin.service;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class EmailService implements InitializingBean {

    JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

    @Value("${email.host}")
    private String emailHost;
    @Value("${email.username}")
    private String emailUsername;
    @Value("${email.password}")
    private String emailPassword;
    @Value("${email.thread.count}")
    private String threadCount;
    private ExecutorService executorService;

    @Override
    public void afterPropertiesSet() throws Exception {
        executorService = Executors.newFixedThreadPool(Integer.parseInt(threadCount));
        javaMailSender.setHost(emailHost);
        javaMailSender.setUsername(emailUsername);
        javaMailSender.setPassword(emailPassword);
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.timeout", "25000");
        javaMailSender.setJavaMailProperties(prop);
    }


    public void sendHtmlMail() {
//        MimeMessage mailMessage = javaMailSender.createMimeMessage();
//        MimeMessageHelper messageHelper = null;
//        try {
//            messageHelper = new MimeMessageHelper(mailMessage, true, "utf-8");
//            // 设置收件人，寄件人
//            messageHelper.setTo("422450455@qq.com");
//            messageHelper.setFrom("weimeittx@126.com");
//            messageHelper.setSubject("文件!");
//            // true 表示启动HTML格式的邮件
//            messageHelper.setText("<html><head></head><body><h1>这是您需要的文件</h1></body></html>", true);
//            messageHelper.addAttachment("jee webserver cluster.pdf", () -> new FileInputStream("C:/Users/Administrator/Desktop/jee webserver cluster.pdf"));
//            javaMailSender.send(mailMessage);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public void sendSimpleMail(String to, String subject, String body) {
        executorService.submit(()->{
            try {
                SimpleMailMessage mailMessage = new SimpleMailMessage();
                mailMessage.setTo(to);
                mailMessage.setFrom(emailUsername);
                mailMessage.setSubject(subject);
                mailMessage.setText(body);
                javaMailSender.send(mailMessage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }
}
