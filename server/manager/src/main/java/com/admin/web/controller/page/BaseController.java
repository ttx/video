package com.admin.web.controller.page;

import com.alibaba.fastjson.JSONObject;
import org.springframework.data.domain.PageRequest;

public abstract class BaseController {
    protected PageRequest convertPage(JSONObject jsonObject) {
        return new PageRequest(jsonObject.getInteger("offset") / jsonObject.getInteger("limit"), jsonObject.getInteger("limit"));
    }
}
