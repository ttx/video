package com.admin.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("test")
@Controller
public class TestController {

  @RequestMapping("test")
  public String test() {
    System.err.println("TestController");
    return "test.jsp";
  }
}
