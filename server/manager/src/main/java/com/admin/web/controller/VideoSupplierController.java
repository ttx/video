package com.admin.web.controller;

import com.admin.bean.HttpResult;
import com.admin.model.VideoSupplier;
import com.admin.repository.mongo.VideoSupplierRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/app/videoSupplier")
public class VideoSupplierController {

    @Resource
    private VideoSupplierRepository videoSupplierRepository;

    @RequestMapping("list")
    public HttpResult<List<VideoSupplier>> list() {
        return HttpResult.buildSuccess(videoSupplierRepository.findAll());
    }
}
