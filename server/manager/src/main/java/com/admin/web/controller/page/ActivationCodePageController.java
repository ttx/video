package com.admin.web.controller.page;

import com.admin.bean.HttpResult;
import com.admin.enums.ActivationCodeState;
import com.admin.model.ActivationCode;
import com.admin.repository.mongo.ActivationCodeRepository;
import com.admin.service.ActivationCodeService;
import com.admin.service.EhCacheService;
import com.admin.utils.ExcelUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/sys/activationCode")
public class ActivationCodePageController {

    @Resource
    private ActivationCodeRepository activationCodeRepository;

    @Resource
    private ActivationCodeService activationCodeService;

    @Resource
    private EhCacheService ehCacheService;

    @Resource
    private MongoTemplate mongoTemplate;

    @RequestMapping("list")
    public String list() {
        return "activationCode/list.html";
    }

    @RequestMapping("/page")
    @ResponseBody
    public Page<ActivationCode> page(@RequestBody JSONObject jsonObject) {
        PageRequest pageRequest = new PageRequest(jsonObject.getInteger("offset") / jsonObject.getInteger("limit"), jsonObject.getInteger("limit"));
        Query query = new Query();
        long count = mongoTemplate.count(query, ActivationCode.class);
        query.with(pageRequest);
        List<ActivationCode> rows = mongoTemplate.find(query, ActivationCode.class);
        return new Page<>(count, rows);
    }

    @RequestMapping("gen")
    @ResponseBody
    public HttpResult<String> gen(Integer num, Long takeEffectTime, Long loseEfficacyTime, Integer timeLimit, Integer price) {
        List<ActivationCode> activationCodes = activationCodeService.activationCodeGen(num, takeEffectTime, loseEfficacyTime, timeLimit, price);
        activationCodeRepository.save(activationCodes);
        for (ActivationCode activationCode : activationCodes) {
            ehCacheService.getSequenceList().add(activationCode.getSequence());
            ehCacheService.getCodeList().add(activationCode.getCode());
        }
        return HttpResult.buildSuccess(activationCodes.get(0).getBatchCode());
    }

    /**
     * 选择
     *
     * @param response
     * @param request
     * @param ids
     * @throws IOException
     */
    @RequestMapping("selectExport")
    @ResponseBody
    public void selectExport(HttpServletResponse response, HttpServletRequest request, String ids) throws IOException {
        List<ActivationCode> list = mongoTemplate.find(Query.query(Criteria.where("id").in(ids.split(","))), ActivationCode.class);
        export(list, request, response, "选择");
    }

    /**
     * 选择
     *
     * @param response
     * @param request
     * @param ids
     * @throws IOException
     */
    @RequestMapping("batchCodeExport")
    @ResponseBody
    public void batchCodeExport(HttpServletResponse response, HttpServletRequest request, String batchCode) throws IOException {
        List<ActivationCode> list = activationCodeRepository.findByBatchCode(batchCode);
        export(list, request, response, "生成");
    }

    /**
     * 所有
     *
     * @param response
     * @param request
     * @throws IOException
     */
    @RequestMapping("allExport")
    @ResponseBody
    public void allExport(HttpServletResponse response, HttpServletRequest request) throws IOException {
        List<ActivationCode> list = activationCodeRepository.findAll();
        export(list, request, response, "所有");
    }

    /**
     * 已激活
     *
     * @param response
     * @param request
     * @throws IOException
     */
    @RequestMapping("activatedExport")
    @ResponseBody
    public void activatedExport(HttpServletResponse response, HttpServletRequest request) throws IOException {
        List<ActivationCode> list = activationCodeRepository.findByState(ActivationCodeState.ACTIVATED.getState());
        export(list, request, response, "已激活");
    }

    /**
     * 未激活
     *
     * @param response
     * @param request
     * @throws IOException
     */
    @RequestMapping("unactivateExport")
    @ResponseBody
    public void unactivateExport(HttpServletResponse response, HttpServletRequest request) throws IOException {
        List<ActivationCode> list = activationCodeRepository.findByState(ActivationCodeState.UNAPPLY.getState());
        export(list, request, response, "未激活");
    }

    /**
     * 已过期
     *
     * @param response
     * @param request
     * @throws IOException
     */
    @RequestMapping("invalidExport")
    @ResponseBody
    public void invalidExport(HttpServletResponse response, HttpServletRequest request) throws IOException {
        List<ActivationCode> list = activationCodeRepository.findByState(ActivationCodeState.EXPIRED.getState());
        export(list, request, response, "已过期");
    }

    private void export(Collection<ActivationCode> list, HttpServletRequest request, HttpServletResponse response, String name) throws IOException {
        ExportParams param = new ExportParams();
        Workbook sheets = ExcelExportUtil.exportExcel(param, ActivationCode.class, list);
        ExcelUtil.download(request, response, sheets, name);
    }
}
