package com.admin.web.controller;

import com.admin.bean.AppUserContext;
import com.admin.bean.HttpResult;
import com.admin.model.AppUser;
import com.admin.model.VideoSupplier;
import com.admin.model.WatchHistory;
import com.admin.repository.mongo.AppUserRepository;
import com.admin.repository.mongo.VideoSupplierRepository;
import com.admin.repository.mongo.WatchHistoryRepository;
import com.admin.utils.ExcelUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

@RequestMapping("/app")
@Controller
public class PlayController {

    @Resource
    private WatchHistoryRepository watchHistoryRepository;

    @Resource
    private VideoSupplierRepository videoSupplierRepository;

    @Resource
    private AppUserRepository appUserRepository;


    @RequestMapping("playable")
    public HttpResult<Void> playable(@RequestBody JSONObject jsonObject) {
        String url = jsonObject.getString("url");
        VideoSupplier videoSupplier = videoSupplierRepository.findOne(jsonObject.getString("id"));
        if (StringUtils.isNotEmpty(url) && videoSupplier != null) {
            try{
                Document doc = Jsoup.parse(new URL(url), 1000);
//                doc.select(videoSupplier.get)
                Elements select = doc.select(videoSupplier.getSelect());
                if(!select.isEmpty()){
                    HttpResult.buildSuccess(null);
                }
            }catch (Exception ex){

            }
        }
        return HttpResult.buildFail(null);
    }


    @RequestMapping("play")
    public String play(String url, String title, String id, String videoId, Model model) {
        VideoSupplier videoSupplier = videoSupplierRepository.findOne(videoId);
        AppUser appUser = AppUserContext.getAppUser();
        appUser = appUserRepository.findOne(appUser.getId());
        if (System.currentTimeMillis() > appUser.getMemberExpire()) {
            return "play/authority.html";
        }
        WatchHistory watchHistory;
        if (StringUtils.isNotEmpty(id)) {
            watchHistory = watchHistoryRepository.findOne(id);
            watchHistory.setWatchTime(System.currentTimeMillis());
        } else {
            watchHistory = new WatchHistory();
            watchHistory.setAppUserId(appUser.getId());
            watchHistory.setTitle(title);
            watchHistory.setVideoId(videoId);
            watchHistory.setUrl(url);
            watchHistory.setPicture(videoSupplier.getIcon());
        }
        watchHistoryRepository.save(watchHistory);


        Map<String, String> convertMap = videoSupplier.getConvertMap();
        for (String key : convertMap.keySet()) {
            String newKey = key.replace(",", ".");
            if (url.startsWith(newKey)) {
                url = url.replace(newKey, convertMap.get(key));
            }
        }

        model.addAttribute("url", url);
        model.addAttribute("interfaceUrl", videoSupplier.getInterfaceUrl());

        model.addAttribute("backupInterfaces", videoSupplier.getBackupInterfaces());
        model.addAttribute("title", title);

        return "play/index.html";
    }


    @RequestMapping("debugPlay")
    public String play(String url, String interfaceUrl, Model model) {
        model.addAttribute("url", url);
        model.addAttribute("interfaceUrl", interfaceUrl);
        return "play/index.html";
    }
}
