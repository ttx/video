package com.admin.web.controller;

import com.admin.bean.AppUserContext;
import com.admin.bean.HttpResult;
import com.admin.enums.ActivationCodeState;
import com.admin.model.ActivationCode;
import com.admin.model.AppUser;
import com.admin.repository.mongo.ActivationCodeRepository;
import com.admin.service.ActivationCodeService;
import com.admin.service.EhCacheService;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/app/ActivationCode")
public class ActivationCodeController {

    @Resource
    private ActivationCodeRepository activationCodeRepository;


    @Resource
    private ActivationCodeService activationCodeService;

    @Resource
    private EhCacheService ehCacheService;

    @Resource
    private MongoTemplate mongoTemplate;

    /**
     * 激活
     *
     * @param activationCode
     * @return
     */
    @RequestMapping("active")
    public HttpResult<ActivationCode> active(@RequestBody ActivationCode activationCode) {
        AppUser appUser = AppUserContext.getAppUser();
        if (appUser == null) {
            return HttpResult.buildFail("请先登陆后在激活!");
        }
        activationCode = activationCodeRepository.findByCode(activationCode.getCode());
        if (activationCode == null) {
            return HttpResult.buildFail("激活码输入错误!");
        }
        if (!activationCode.getState().equals(ActivationCodeState.UNAPPLY.getState())) {
            return HttpResult.buildFail("该激活码已被使用!");
        }
        if (activationCode.getTakeEffectTime() > System.currentTimeMillis()) {
            return HttpResult.buildFail("该激活码还未到使用时间!");
        }

        if (System.currentTimeMillis() > activationCode.getLoseEfficacyTime()) {
            return HttpResult.buildFail("该激活码已过期!");
        }
        activationCode.setState(ActivationCodeState.EXPIRED.getState());
        activationCode.setActiveTime(System.currentTimeMillis());
        activationCode.setOwner(appUser.getId());
        activationCodeRepository.save(activationCode);
        Long memberExpire = appUser.getMemberExpire();
        if (memberExpire == null || memberExpire < System.currentTimeMillis()) {
            memberExpire = System.currentTimeMillis();
        }
        memberExpire = DateUtils.addHours(new Date(memberExpire), activationCode.getTimeLimit()).getTime();



        mongoTemplate.updateFirst(Query.query(Criteria.where("id").is(appUser.getId())), Update.update("memberExpire", memberExpire), AppUser.class);

//        mongoTemplate.updateMulti(Query.query(Criteria.where("id").is(appUser.getId())), Update.update("memberExpire", memberExpire), AppUser.class);

        return HttpResult.buildSuccess(activationCode);
    }


}
