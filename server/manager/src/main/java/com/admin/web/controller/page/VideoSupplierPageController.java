package com.admin.web.controller.page;

import com.admin.bean.HttpResult;
import com.admin.model.ActivationCode;
import com.admin.model.VideoSupplier;
import com.alibaba.fastjson.JSONObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/sys/videoSupplier")
public class VideoSupplierPageController extends BaseController {
    @Resource
    private MongoTemplate mongoTemplate;

    @RequestMapping("list")
    public String list() {
        return "videoSupplier/list.html";
    }

    @RequestMapping("/page")
    @ResponseBody
    public Page<VideoSupplier> page(@RequestBody JSONObject jsonObject) {
        PageRequest pageRequest = convertPage(jsonObject);
        Query query = new Query();
        long count = mongoTemplate.count(query, VideoSupplier.class);
        query.with(pageRequest);
        List<VideoSupplier> rows = mongoTemplate.find(query, VideoSupplier.class);
        return new Page<>(count, rows);
    }

    @RequestMapping("updateInterfaceUrl")
    @ResponseBody
    public HttpResult<Void> updateInterfaceUrl(@RequestBody VideoSupplier videoSupplier){
        Update update = Update.update("interfaceUrl", videoSupplier.getInterfaceUrl()).set("backupInterfaces", videoSupplier.getBackupInterfaces());
        mongoTemplate.updateFirst(Query.query(Criteria.where("id").is(videoSupplier.getId())), update,VideoSupplier.class);
        return HttpResult.buildSuccess(null);
    }
}
