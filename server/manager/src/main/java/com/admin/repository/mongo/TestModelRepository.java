package com.admin.repository.mongo;

import com.admin.model.TestModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TestModelRepository extends MongoRepository<TestModel, String> {
}
