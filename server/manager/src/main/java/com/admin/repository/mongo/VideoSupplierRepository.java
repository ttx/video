package com.admin.repository.mongo;

import com.admin.model.VideoSupplier;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface VideoSupplierRepository extends MongoRepository<VideoSupplier, String> {
}
