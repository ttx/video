package com.admin.repository.mongo;

import com.admin.model.AppUser;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AppUserRepository extends MongoRepository<AppUser, String> {
    AppUser findByEmailAndPassword(String email, String password);
    AppUser findByEmail(String email);
    AppUser findByIdAndSerial(String id, String serial);
}
