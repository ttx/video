package com.admin.repository.mongo;

import com.admin.model.AppUp;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AppUpRepository extends MongoRepository<AppUp, String> {
}
