package com.admin.repository.mongo;

import com.admin.model.ActivationCode;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ActivationCodeRepository extends MongoRepository<ActivationCode, String> {
    ActivationCode findByCode(String code);

    List<ActivationCode> findByState(Integer state);

    List<ActivationCode> findByBatchCode(String batchCode);

}
