package com.admin.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Administrator on 2017/2/20.
 */
@Document
public class TestModel {
	@Id
	private String id;

	private String name;
	private double[] location;
	@GeoSpatialIndexed
	private GeoJsonPoint point;

	public GeoJsonPoint getPoint() {
		return point;
	}

	public void setPoint(GeoJsonPoint point) {
		this.point = point;
	}

	public double[] getLocation() {
		return location;
	}

	public void setLocation(double[] location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
