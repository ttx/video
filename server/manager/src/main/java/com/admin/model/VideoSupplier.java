package com.admin.model;

import com.alibaba.fastjson.annotation.JSONField;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 视频供应商
 */
@Document
public class VideoSupplier {

    @Id
    @JSONField(name = "_id")
    private String id;

    /**
     * 地址
     */
    private String url;
    /**
     * 图标地址
     */
    private String icon;

    /**
     * 名字
     */
    private String label;

    /**
     * 注入的js脚本
     */
    private String script;


    /**
     * 接口地址列表
     */
    private String interfaceUrl;

    /**
     * 是否支持
     */
    private Boolean support;

    /**
     * 备份接口地址
     */
    private List<BackupInterface> backupInterfaces = new ArrayList<>();

    /**
     * 播放页面元素标识
     */
    private String select;
    /**
     * URL 转化
     * <p>
     * http://m.iqiyi.com/ -> http://www.iqiyi.com/
     */
    private HashMap<String, String> convertMap = new HashMap<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getInterfaceUrl() {
        return interfaceUrl;
    }

    public void setInterfaceUrl(String interfaceUrl) {
        this.interfaceUrl = interfaceUrl;
    }

    public HashMap<String, String> getConvertMap() {
        return convertMap;
    }

    public void setConvertMap(HashMap<String, String> convertMap) {
        this.convertMap = convertMap;
    }

    public Boolean getSupport() {
        return support;
    }

    public void setSupport(Boolean support) {
        this.support = support;
    }

    public List<BackupInterface> getBackupInterfaces() {
        return backupInterfaces;
    }

    public void setBackupInterfaces(List<BackupInterface> backupInterfaces) {
        this.backupInterfaces = backupInterfaces;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }
}
