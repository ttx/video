package com.admin.model;

import com.alibaba.fastjson.annotation.JSONField;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * APP用户
 */
@Document
public class AppUser {
    @Id
    private String id;
    /**
     * 头像
     */
    private String head;
    /**
     * 是否会员
     */
    private Boolean isMember = false;

    /**
     * 到期时间
     */
    private Long memberExpire = System.currentTimeMillis();
    /**
     * 邮箱
     */
    private String email;
    /**
     * 密码
     */
    @JSONField(serialize = false)
    private String password;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 手机号
     */
    private String phone;

    private Long registerTime;

    /**
     * 设备序列号
     */
    private String serial;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public Boolean getMember() {
        return isMember;
    }

    public void setMember(Boolean member) {
        isMember = member;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Long registerTime) {
        this.registerTime = registerTime;
    }

    public Long getMemberExpire() {
        return memberExpire;
    }

    public void setMemberExpire(Long memberExpire) {
        this.memberExpire = memberExpire;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
}
