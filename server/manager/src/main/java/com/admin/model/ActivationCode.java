package com.admin.model;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * 激活码
 */
@Document
@ExcelTarget("ActivationCode")
public class ActivationCode {
    @Id
    private String id;

    /**
     * 序列号
     */
    @Excel(name = "序列号", orderNum = "1")
    private String sequence;
    /**
     * code
     */
    @Excel(name = "激活码", orderNum = "2", width = 18)
    private String code;

    /**
     * 生效日期
     */
    private Long takeEffectTime;

    /**
     * 生效日期
     */
    @Excel(name = "生效日期", orderNum = "3", format = "yyyy-MM-dd HH:mm", width = 18)
    private Date takeEffectDate;


    /**
     * 失效日期
     */
    private Long loseEfficacyTime;

    /**
     * 失效日期
     */
    @Excel(name = "失效日期", orderNum = "4", format = "yyyy-MM-dd HH:mm", width = 18)
    private Date loseEfficacyDate;

    /**
     * 激活时间
     */
    private Long activeTime;

    /**
     * 激活时间
     */
    @Excel(name = "激活时间", orderNum = "5", format = "yyyy-MM-dd HH:mm")
    private Date activeDate;

    /**
     * 激活人
     */
    private String owner;
    /**
     * 期限 单位/小时
     */
    @Excel(name = "期限(单位/小时)", orderNum = "6", suffix = "小时")
    private Integer timeLimit;
    /**
     * 状态 0:未激活,1:已激活,2:已过期
     */
    @Excel(name = "状态(0:未激活,1:已激活,2:已过期)", orderNum = "7", width = 18)
    private Integer state = 0;

    /**
     * 批次
     */
    private String batchCode;


    /**
     * 价格
     */
    private Integer price;


    @Excel(name = "是否出售", orderNum = "8")
    private Boolean sale = false;

    public ActivationCode() {

    }

    public ActivationCode(String code, String sequence, Long takeEffectTime, Long loseEfficacyTime, Integer timeLimit, String batchCode) {
        this.code = code;
        this.sequence = sequence;
        this.takeEffectTime = takeEffectTime;
        this.loseEfficacyTime = loseEfficacyTime;
        this.timeLimit = timeLimit;
        this.batchCode = batchCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getTakeEffectTime() {
        return takeEffectTime;
    }

    public void setTakeEffectTime(Long takeEffectTime) {
        this.takeEffectTime = takeEffectTime;
    }

    public Long getLoseEfficacyTime() {
        return loseEfficacyTime;
    }

    public void setLoseEfficacyTime(Long loseEfficacyTime) {
        this.loseEfficacyTime = loseEfficacyTime;
    }

    public Integer getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(Integer timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public Long getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(Long activeTime) {
        this.activeTime = activeTime;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public Date getTakeEffectDate() {
        return new Date(takeEffectTime);
    }

    public Date getLoseEfficacyDate() {
        return new Date(loseEfficacyTime);
    }

    public Date getActiveDate() {
        if (activeTime == null) {
            return null;
        }
        return new Date(activeTime);
    }

    public Boolean getSale() {
        return sale;
    }

    public void setSale(Boolean sale) {
        this.sale = sale;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
