package com.admin.model;

public class BackupInterface {
    private String label;
    private String url;

    public BackupInterface() {
    }

    public BackupInterface(String label, String url) {
        this.label = label;
        this.url = url;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
