package com.admin.bean;

import com.admin.model.AppUser;

public class AppUserContext {
    private static ThreadLocal<AppUser> appUserThreadLocal = new ThreadLocal<>();

    public static void setAppUser(AppUser appUser) {
        appUserThreadLocal.set(appUser);
    }

    public static AppUser getAppUser() {
        return appUserThreadLocal.get();
    }

    public static void clearAppUser() {
        appUserThreadLocal.remove();
    }
}
