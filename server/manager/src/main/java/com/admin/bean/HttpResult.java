package com.admin.bean;

public class HttpResult<T> {
    private Boolean success = true;
    private String msg;
    private T result;
    public HttpResult() {
    }
    public HttpResult(T result) {
        super();
        this.result = result;
    }
    public HttpResult(String msg) {
        super();
        this.msg = msg;
    }
    public static <R> HttpResult<R> buildSuccess(R t) {
        return new HttpResult<R>(t);
    }
    public static <R> HttpResult<R> buildFail(String msg) {
        HttpResult<R> httpResult = new HttpResult<>(msg);
        httpResult.setSuccess(false);
        return httpResult;
    }
    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

}