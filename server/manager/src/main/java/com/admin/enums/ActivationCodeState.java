package com.admin.enums;

public enum ActivationCodeState {

    UNAPPLY(0, "未激活"), ACTIVATED(1, "已激活"), EXPIRED(2, "已过期");
    private Integer state;
    private String desc;

    ActivationCodeState(Integer state, String desc) {
        this.state = state;
        this.desc = desc;
    }

    public Integer getState() {
        return state;
    }

    public String getDesc() {
        return desc;
    }
}

